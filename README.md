# tela-circle-icon-theme

A flat, colorful icon theme

https://github.com/vinceliuice/Tela-circle-icon-theme

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/icons-and-themes/tela-circle-icon-theme.git
```

<br>

# Icons preview:

<p align="center">
<img src="https://gitlab.com/azul4/images/tela-circle-icon-theme/-/raw/main/tela-circle-icon-theme-black.png">
</p>

<br>

<p align="center">
<img src="https://gitlab.com/azul4/images/tela-circle-icon-theme/-/raw/main/tela-circle-icon-theme-white.png">
</p>
